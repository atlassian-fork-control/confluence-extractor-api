package com.atlassian.confluence.plugins.index.api;

/**
 * Stores binary value in a columnar form.
 */
public final class DocValuesFieldDescriptor extends FieldDescriptor {
    private final byte[] bytesValue;

    public DocValuesFieldDescriptor(String name, byte[] value) {
        super(name, null, Store.DOC_VALUES, Index.NO);
        this.bytesValue = value;
    }

    public <T> T accept(FieldVisitor<T> fieldVisitor) {
        return fieldVisitor.visit(this);
    }

    public byte[] bytesValue() {
        return bytesValue;
    }
}
