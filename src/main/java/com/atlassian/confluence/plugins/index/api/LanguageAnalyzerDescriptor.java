package com.atlassian.confluence.plugins.index.api;

import com.atlassian.annotations.ExperimentalApi;

import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

/**
 * An analyzer, that can behave differently depending on a given language.
 *
 * @since 2.0.6
 */
@ExperimentalApi
public final class LanguageAnalyzerDescriptor implements AnalyzerDescriptorProvider {
    private final AnalyzerDescriptor analyzer;
    private final Map<LanguageDescriptor, AnalyzerDescriptor> languageSpecificAnalyzers;

    private LanguageAnalyzerDescriptor(Builder builder) {
        analyzer = builder.analyzer;
        languageSpecificAnalyzers = new HashMap<>(builder.languageSpecificAnalyzers);
    }

    @Override
    public Optional<AnalyzerDescriptor> getAnalyzer(LanguageDescriptor language) {
        return Optional.ofNullable(languageSpecificAnalyzers.getOrDefault(language, analyzer));
    }

    /**
     * Create a builder for language dependent analyzer with the given default analyzer.
     */
    public static Builder builder(AnalyzerDescriptor analyzer) {
        return new Builder(analyzer);
    }

    /**
     * Create a builder for language dependent analyzer without default analyzer.
     */
    public static Builder builder() {
        return new Builder(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LanguageAnalyzerDescriptor)) return false;
        LanguageAnalyzerDescriptor that = (LanguageAnalyzerDescriptor) o;
        return analyzer.equals(that.analyzer) &&
                languageSpecificAnalyzers.equals(that.languageSpecificAnalyzers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(analyzer, languageSpecificAnalyzers);
    }

    public static class Builder {
        private final AnalyzerDescriptor analyzer;
        private final Map<LanguageDescriptor, AnalyzerDescriptor> languageSpecificAnalyzers;

        private Builder(@Nullable AnalyzerDescriptor analyzer) {
            this.analyzer = analyzer;
            this.languageSpecificAnalyzers = new HashMap<>();
        }

        public Builder analyzer(LanguageDescriptor language, AnalyzerDescriptor analyzer) {
            languageSpecificAnalyzers.put(language, analyzer);
            return this;
        }

        public LanguageAnalyzerDescriptor build() {
            return new LanguageAnalyzerDescriptor(this);
        }
    }
}
