package com.atlassian.confluence.plugins.index.api;

import java.util.Collection;

/**
 * This interface provides clients with the ability add additional fields and body text to the index documents produced
 * for each searchable object in Confluence.
 */
public interface Extractor2 {
    /**
     * Returns a buffer of text that will be appended onto the end of a larger buffer of text that will eventually
     * become the searchable body text.
     *
     * @param searchable a searchable object
     * @return Returns a buffer of text that will be appended onto the end of a larger buffer of text that will eventually
     * become the searchable body text.
     */
    StringBuilder extractText(Object searchable);

    /**
     * Extract fields from the searchable object.
     *
     * @param searchable a searchable object
     * @return list of fields extracted from the searchable object
     */
    Collection<FieldDescriptor> extractFields(Object searchable);
}