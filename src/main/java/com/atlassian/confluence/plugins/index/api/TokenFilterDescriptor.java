package com.atlassian.confluence.plugins.index.api;

import com.atlassian.annotations.ExperimentalApi;

/**
 * A token filter must be predefined in Confluence. It is a capability of a product and not something that
 * plugin can change. A plugin however can combine these predefined filters in {@link AnalyzerDescriptor} to achieve
 * a desire effect.
 *
 * @since 2.0.2
 */
@ExperimentalApi
public interface TokenFilterDescriptor {
}
